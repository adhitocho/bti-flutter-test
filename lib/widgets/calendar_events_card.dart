import 'package:bti_test_flutter/config/themes/color_theme.dart';
import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:flutter/material.dart';

class CalendarEventsCard extends StatelessWidget {
  final Map data;

  const CalendarEventsCard({
    super.key,
    required this.data,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        height: 80,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: greyColor),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  data["title"],
                  style: bodyLgTextStyle.copyWith(fontWeight: FontWeight.w700),
                ),
                Text(
                  data["date"],
                  style: bodyLgTextStyle,
                ),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              data["desc"],
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
