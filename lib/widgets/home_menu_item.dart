import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:flutter/material.dart';

class HomeMenuItem extends StatelessWidget {
  final String title;
  const HomeMenuItem({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return ListTile(
        tileColor: Theme.of(context).colorScheme.primary,
        titleAlignment: ListTileTitleAlignment.center,
        onTap: () {},
        title: Center(
          child: Text(
            title,
            style: bodyTextStyle.copyWith(
                color: Theme.of(context).colorScheme.tertiary),
          ),
        ));
  }
}
