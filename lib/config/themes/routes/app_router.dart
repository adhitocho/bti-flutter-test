import 'package:auto_route/auto_route.dart';
import 'package:bti_test_flutter/app/home/home_screen.dart';
import 'package:bti_test_flutter/app/landing/landing_screen.dart';
import 'package:bti_test_flutter/app/login/login_screen.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {

  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: '/',
          page: LandingRoute.page,
        ),
        AutoRoute(
          path: '/home',
          page: HomeRoute.page,
        ),
        AutoRoute(
          path: '/login',
          page: LoginRoute.page,
        ),
      ];
}
