import 'package:auto_route/auto_route.dart';
import 'package:bti_test_flutter/config/themes/images.dart';
import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

@RoutePage()
class LandingScreen extends StatelessWidget {
  const LandingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        height: screenHeight,
        width: screenWidth,
        color: Theme.of(context).colorScheme.primary,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'EDUCATION',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.tertiary,
                  fontWeight: FontWeight.w700,
                  fontSize: 24,
                  letterSpacing: 8),
            ),
            const SizedBox(
              height: 50,
            ),
            SvgPicture.asset(
              booksStackImg,
              width: 200,
            ),
            const SizedBox(
              height: 50,
            ),
            Text(
              'Doler ipsum dolor sit amet asyimimiw pirininiw kucirinimiw',
              style: titleSmTextStyle.copyWith(
                  color: Theme.of(context).colorScheme.tertiary),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              onPressed: () {
                AutoRouter.of(context).pushNamed("/login");
              },
              child: Text(
                'START',
                style: headlineTextStyle.copyWith(letterSpacing: 8),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
