import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:flutter/material.dart';

class CommunityTabScreen extends StatefulWidget {
  const CommunityTabScreen({super.key});

  @override
  State<CommunityTabScreen> createState() => _CommunityTabScreenState();
}

class _CommunityTabScreenState extends State<CommunityTabScreen> {
  @override
  Widget build(BuildContext context) {
    List communityEvents = [
      {"id": 1, "title": "Event 1", "desc": "Desc 1"},
      {"id": 2, "title": "Event 2", "desc": "Desc 2"},
      {"id": 3, "title": "Event 3", "desc": "Desc 3"},
      {"id": 4, "title": "Event 4", "desc": "Desc 4"},
      {"id": 5, "title": "Event 5", "desc": "Desc 5"},
      {"id": 6, "title": "Event 6", "desc": "Desc 6"},
      {"id": 7, "title": "Event 7", "desc": "Desc 7"},
      {"id": 8, "title": "Event 8", "desc": "Desc 8"},
      {"id": 9, "title": "Event 9", "desc": "Desc 9"},
      {"id": 10, "title": "Event 10", "desc": "Desc 10"},
      {"id": 11, "title": "Event 11", "desc": "Desc 11"},
      {"id": 12, "title": "Event 12", "desc": "Desc 12"}
    ];

    return Container(
      margin: const EdgeInsets.only(top: 80),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            'Lorem Ipsum',
            style: headlineTextStyle,
            textAlign: TextAlign.center,
          ),
          Container(
            margin: const EdgeInsets.only(top: 20),
            padding: const EdgeInsets.symmetric(horizontal: 40),
            height: 450,
            child: GridView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  mainAxisExtent: 110,
                ),
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: communityEvents.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(
                        color: Theme.of(context).colorScheme.secondary,
                        width: 2,
                      ),
                    ),
                    child: Stack(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              communityEvents[index]["title"],
                              style: titleSmTextStyle.copyWith(
                                  fontWeight: FontWeight.w700),
                            ),
                            Text(communityEvents[index]["desc"]),
                          ],
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Icon(
                            Icons.verified,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        )
                      ],
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
