import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:bti_test_flutter/widgets/home_menu_item.dart';
import 'package:flutter/material.dart';

class HomeTabScreen extends StatelessWidget {
  const HomeTabScreen({super.key});

  @override
  Widget build(BuildContext context) {
    List menuItem = [
      'Menu 1',
      'Menu 2',
      'Menu 3',
      'Menu 4',
      'Menu 5',
      'Menu 6',
    ];

    return Container(
      margin: const EdgeInsets.only(top: 100),
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.secondary,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12))),
            child: Row(
              children: [
                Icon(
                  Icons.person,
                  color: Theme.of(context).colorScheme.tertiary,
                  size: 45,
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Welcome, John Cena',
                      style: bodyLgTextStyle.copyWith(
                          color: Theme.of(context).colorScheme.tertiary,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Kacang Hijau',
                      style: labelTextStyle.copyWith(
                          color: Theme.of(context).colorScheme.tertiary),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: ((60 + 1) * menuItem.length).toDouble(),
            child: ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              itemCount: menuItem.length,
              itemBuilder: (BuildContext context, int index) {
                return HomeMenuItem(
                  title: menuItem[index],
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  height: 1,
                  color: Colors.white,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
