import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:bti_test_flutter/widgets/calendar_events_card.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class ScheduleTabScreen extends StatefulWidget {
  const ScheduleTabScreen({super.key});

  @override
  State<ScheduleTabScreen> createState() => ScheduleTabScreenState();
}

class ScheduleTabScreenState extends State<ScheduleTabScreen> {
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;

  List events = [
    {
      "id": 1,
      "title": "Event 1",
      "desc": "Lorem Ipsum syimiw ",
      "date": "2024-10-10"
    },
    {
      "id": 2,
      "title": "Event 2",
      "desc": "Lorem Ipsum syimiw",
      "date": "2024-11-11"
    },
    {
      "id": 3,
      "title": "Event 3",
      "desc": "Lorem Ipsum syimiw",
      "date": "2024-12-12"
    },
    {
      "id": 4,
      "title": "Event 4",
      "desc":
          "Ada sebuah kisah Tentang dara jelita Hidup s'lalu penuh Dera dan siksa",
      "date": "2025-01-01"
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 80),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            'Lorem Ipsum',
            style: headlineTextStyle,
            textAlign: TextAlign.center,
          ),
          Container(
            margin: const EdgeInsets.only(top: 10),
            decoration: BoxDecoration(
              border:
                  Border.all(color: Theme.of(context).colorScheme.secondary),
              borderRadius: BorderRadius.circular(10),
            ),
            child: TableCalendar(
              firstDay: DateTime.utc(2010, 10, 16),
              lastDay: DateTime.utc(2030, 3, 14),
              focusedDay: _focusedDay,
              calendarFormat: _calendarFormat,
              selectedDayPredicate: (day) {
                return isSameDay(_selectedDay, day);
              },
              onDaySelected: (selectedDay, focusedDay) {
                if (!isSameDay(_selectedDay, selectedDay)) {
                  setState(() {
                    _selectedDay = selectedDay;
                    _focusedDay = focusedDay;
                  });
                }
              },
              onFormatChanged: (format) {
                if (_calendarFormat != format) {
                  setState(() {
                    _calendarFormat = format;
                  });
                }
              },
              onPageChanged: (focusedDay) {
                _focusedDay = focusedDay;
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20),
            height: 100,
            child: ListView.separated(
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return CalendarEventsCard(
                    data: events[index],
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const SizedBox(
                    height: 10,
                  );
                },
                itemCount: events.length),
          ),
        ],
      ),
    );
  }
}
