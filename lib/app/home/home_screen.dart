import 'package:auto_route/annotations.dart';
import 'package:bti_test_flutter/app/home/tabs/community_tab/community_tab_screen.dart';
import 'package:bti_test_flutter/app/home/tabs/home_tab/home_tab_screen.dart';
import 'package:bti_test_flutter/app/home/tabs/schedule_tab/schedule_tab_screen.dart';
import 'package:bti_test_flutter/config/themes/color_theme.dart';
import 'package:bti_test_flutter/config/themes/images.dart';
import 'package:flutter/material.dart';

@RoutePage()
class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  int selectedHomeTab = 0;
  List<Widget> tabScreens = [
    const HomeTabScreen(),
    const ScheduleTabScreen(),
    const HomeTabScreen(),
    const CommunityTabScreen(),
    const HomeTabScreen(),
  ];
  late TabController homeTabController;

  @override
  void initState() {
    homeTabController = TabController(length: tabScreens.length, vsync: this);
    super.initState();
  }

  void selectTab(int index) {
    homeTabController.animateTo(index);
    setState(() {
      selectedHomeTab = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Positioned(
            top: -30,
            child: Image.asset(
              selectedHomeTab % 2 == 0 ? headerOrangeImg : headerPurpleImg,
              fit: BoxFit.fitWidth,
            ),
          ),
          SafeArea(
            child: TabBarView(
              controller: homeTabController,
              physics: const NeverScrollableScrollPhysics(),
              children: tabScreens,
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: selectedHomeTab,
        onTap: selectTab,
        selectedItemColor: Theme.of(context).colorScheme.primary,
        unselectedItemColor: greyColor,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_month), label: 'Schedule'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
          BottomNavigationBarItem(
              icon: Icon(Icons.recycling_rounded), label: 'Community'),
          BottomNavigationBarItem(icon: Icon(Icons.menu), label: 'Settings'),
        ],
      ),
    );
  }
}
